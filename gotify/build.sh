#!/bin/bash

VERSION=$1

if [[ "$VERSION" == "" ]]; then
  echo "add as parameter the number version to build";
  exit;
fi


docker build -f Dockerfile.arm64v8 -t fredix/arm64v8-gotify:$VERSION .
docker push fredix/arm64v8-gotify:$VERSION
